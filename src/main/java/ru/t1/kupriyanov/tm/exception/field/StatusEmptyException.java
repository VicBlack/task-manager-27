package ru.t1.kupriyanov.tm.exception.field;

public final class StatusEmptyException extends AbstractFiledException {

    public StatusEmptyException() {
        super("Error! Project status is empty...");
    }

}
