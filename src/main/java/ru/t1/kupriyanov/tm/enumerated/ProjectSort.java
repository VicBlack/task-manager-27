package ru.t1.kupriyanov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.comparator.CreatedComparator;
import ru.t1.kupriyanov.tm.comparator.NameComparator;
import ru.t1.kupriyanov.tm.comparator.StatusComparator;
import ru.t1.kupriyanov.tm.model.Project;

import java.util.Comparator;

public enum ProjectSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<Project> comparator;

    @Nullable
    public static ProjectSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final ProjectSort projectSort : values()) {
            if (projectSort.name().equals(value)) return projectSort;
        }
        return null;
    }

    ProjectSort(@NotNull String displayName, @NotNull Comparator<Project> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator<Project> getComparator() {
        return comparator;
    }

}
